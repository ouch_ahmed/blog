package aston.blog.fr;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AuteurTest {

	private static List<Auteur> auteurs = new ArrayList<Auteur>();
	
	@BeforeEach
	void peuplerLesAuteurs() {
		Auteur auteurA = new Auteur("PEIXOTO", "Julien", LocalDate.of(2021,6,21), "21 rue machin 90000", "julien@email.com", "1234", StatutUtilisateur.ACTIVE, null);
		Auteur auteurB = new Auteur("OUCHAOUA", "Ahmed", LocalDate.of(2021,6,21), "21 rue machin 90000", "ahmed@email.com", "123", StatutUtilisateur.ACTIVE, null);
		Auteur auteurC = new Auteur("CHAUVIN", "Mathieu", LocalDate.of(2021,6,21), "21 rue machin 90000", "mathieu@email.com", "12", StatutUtilisateur.ACTIVE, null);
		auteurs.add(auteurA);
		auteurs.add(auteurB);
		auteurs.add(auteurC);
	}
	
	@Test
	void testLogin() {
		String mail = "julien@email.com";
		String mdp = "1234";
		Auteur auteur = new Auteur();
		auteur.setAuteurs(auteurs);
		Auteur auteurAuthentifie = auteur.login(mail, mdp);
		assertNotNull(auteurAuthentifie);
		assertEquals(auteurAuthentifie.getNom(), "PEIXOTO");
		assertEquals(auteurAuthentifie.getPrenom(), "Julien");
	}

}
