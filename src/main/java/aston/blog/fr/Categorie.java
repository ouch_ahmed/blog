package aston.blog.fr;

import java.util.List;

public class Categorie {
	private String description;
	private List<Article> articles;
	
	public Categorie(String description, List<Article> articles) {
		super();
		this.description = description;
		this.articles = articles;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
}
