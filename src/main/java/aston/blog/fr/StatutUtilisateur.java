package aston.blog.fr;

public enum StatutUtilisateur {
	ACTIVE("Activé"),
	DESACTIVE("Désactivé");
	
	private final String value;
	
	StatutUtilisateur(String statut) {
		this.value = statut;
	}

	public String getValue() {
		return this.value;
	}
	
}
