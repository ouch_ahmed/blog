package aston.blog.fr;

import java.time.LocalDate;
import java.util.List;

public class Auteur {
	private String nom;
	private String prenom;
	private LocalDate ddn;
	private String adresse;
	private String email;
	private String password;
	private StatutUtilisateur statut;
	private List<Article> articles;
	private static List<Auteur> auteurs = null;

	public Auteur(String nom, String prenom, LocalDate ddn, String adresse, String email, String password,
			StatutUtilisateur statut, List<Article> articles) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.ddn = ddn;
		this.adresse = adresse;
		this.email = email;
		this.password = password;
		this.statut = statut;
		this.articles = articles;
	}

	public Auteur() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDdn() {
		return ddn;
	}

	public void setDdn(LocalDate ddn) {
		this.ddn = ddn;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public StatutUtilisateur getStatut() {
		return statut;
	}

	public void setStatut(StatutUtilisateur statut) {
		this.statut = statut;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}


	public List<Auteur> getAuteurs() {
		return auteurs;
	}

	public void setAuteurs(List<Auteur> auteurs) {
		Auteur.auteurs = auteurs;
	}

	public Auteur login(String mail, String mdp) {
		Auteur auteur = null;
		for(Auteur a : this.auteurs) {
			if(a.getPassword().equals(mdp) && a.getEmail().equals(mail)) {
				auteur = a;
			}
		}
		return auteur;
	}


}
