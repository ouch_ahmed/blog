package aston.blog.fr;

public class Commentaire {
	private String description;
	private Article article;
	
	public Commentaire(String description, Article article) {
		super();
		this.description = description;
		this.article = article;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

}
