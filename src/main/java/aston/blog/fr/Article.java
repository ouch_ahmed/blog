package aston.blog.fr;

import java.time.LocalDateTime;
import java.util.List;

public class Article {

	private String description;
	private LocalDateTime date_publication;
	private Integer nombre_aime;
	private Commentaire commentaire;
	private Auteur auteur;
	private List<Commentaire> commentaires;
	private Categorie categorie;
	
	public Article(String description, LocalDateTime date_publication, Integer nombre_aime, Commentaire commentaire,
			Auteur auteur, List<Commentaire> commentaires, Categorie categorie) {
		super();
		this.description = description;
		this.date_publication = date_publication;
		this.nombre_aime = nombre_aime;
		this.commentaire = commentaire;
		this.auteur = auteur;
		this.commentaires = commentaires;
		this.categorie = categorie;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getDate_publication() {
		return date_publication;
	}

	public void setDate_publication(LocalDateTime date_publication) {
		this.date_publication = date_publication;
	}

	public Integer getNombre_aime() {
		return nombre_aime;
	}

	public void setNombre_aime(Integer nombre_aime) {
		this.nombre_aime = nombre_aime;
	}

	public Commentaire getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(Commentaire commentaire) {
		this.commentaire = commentaire;
	}

	public Auteur getAuteur() {
		return auteur;
	}

	public void setAuteur(Auteur auteur) {
		this.auteur = auteur;
	}

	public List<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
			
}
